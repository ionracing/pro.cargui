﻿using Ion.Data.Networking.Logging;
using Ion.Data.Networking.Manager;
using Ion.Data.Sensor;
using Ion.Graphics.IonEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.CarGui
{
    public class DataReciver
    {
        public DataReciver()
        {

        }
        //static LogManager manager;
        static LogInterface receiveLogger;
        static LogInterface transmitLogger;
        static LogInterface canLogger;
        static LogInterface gpsLogger;

        public static bool DriveToSide = false;
        public static DateTime MessageReceived = DateTime.Now;
        static SerialPort xbeePort;
        static SerialPort piPort;
        static SerialPort gpsPort;
        public static CanNetworkClient canClient;

        public static DataClient carClient;
        public static DataClient telemetryClient;

        public static bool running = false;

        static FileWriter usartLogWriter;
        static FileWriter exceptionWriter;

        static Dictionary<SensorLookup, DataWrapper> lastesTable = new Dictionary<SensorLookup, DataWrapper>();

        public static DataWrapper? GetData(ushort sensorID)
        {
            return GetData(SensorLookup.GetById(sensorID));
        }

        public static DataWrapper? GetData(string name)
        {
            return GetData(SensorLookup.GetByName(name));
        }

        public static DataWrapper? GetData(SensorLookup lookup)
        {
            if (lookup == null)
                return null;
            if (lastesTable.ContainsKey(lookup))
                return lastesTable[lookup];
            return null;
        }

        public static void Initialize()
        {
            LogManager manager = new LogManager();
            manager.OnVerbose += (object sender, LogWriteEventArgs e) => Console.WriteLine("(" + e.Entry.Time.ToString("HH:mm:ss") + ")[" + e.Entry.Sender + "]\t" + e.Entry.Value);
            Initialize(manager);
        }

        public static void Initialize(LogManager manager)
        {
            //TODO: Uncomment when on pi
            string logDir = "/home/pi/log/";
            if (Environment.MachineName == "nicolas-virtual-machine")
                logDir = "/home/nicolas/log/";

            if (!Directory.Exists(logDir))
                Directory.CreateDirectory(logDir);
            int counter = 0;
            while (File.Exists(logDir + counter.ToString("000") + "_usart_data.log") || File.Exists(logDir + counter.ToString("000") + "_usart_exception.log"))
                counter++;

            byte[] prevMessage = new byte[0];
            if (File.Exists(logDir + (counter - 1).ToString("000") + "_usart_exception.log"))
                prevMessage = File.ReadAllBytes(logDir + (counter - 1).ToString("000") + "_usart_exception.log");
            LogInterface prevCan = manager.CreateLogger("CANOLDMSG");
            if (prevMessage.Length > 0)
            {
                for (int i = 0; i < prevMessage.Length; i += 3)
                {
                    prevCan.WriteCustom(new CanEntry() { CanMessage = DataWrapper.ReadData(prevMessage, i, true), Category = "ERROR", Level = 4, Value = "Previous CAN error" });
                    //canLogger.WriteCustom(new CanErrorEntry() { CanMessage = wrapper, Category = "ERROR", Level = 3, Value = "CAN error message"});
                }
            }

            usartLogWriter = new FileWriter(logDir + counter.ToString("000") + "_usart_data.log");
            exceptionWriter = new FileWriter(logDir + counter.ToString("000") + "_usart_exception.log");

            receiveLogger = manager.CreateLogger("RECEIVER");
            transmitLogger = manager.CreateLogger("TRANSMIT");
            canLogger = manager.CreateLogger("CANMSG");
            gpsLogger = manager.CreateLogger("GPSINFO");


            //canLogger.WriteCustom(new CanEntry() { CanMessage = new DataWrapper() { SensorID = 101, Value = 37 }, Category = "ERROR", Level = 4, });
            /*for (int i = 0; i < 10; i++)
            {
                DataWrapper wrap = new DataWrapper() { SensorID = 101, Value = 38 };

                usartLogWriter.Write(wrap.GetBytes(true));
                exceptionWriter.Write(wrap.GetBytes(true));
                System.Threading.Thread.Sleep(500);
            }*/

            try
            {
                if (Contains(SerialPort.GetPortNames(), "/dev/ttyUSB0"))
                {
                    xbeePort = new SerialPort("/dev/ttyUSB0", 19200, Parity.None, 8, StopBits.One);
                    xbeePort.Open();
                    telemetryClient = new UdpNetworkClient(xbeePort, 150) { ShortMode = true };
                    (telemetryClient as UdpNetworkClient).Connect(151);
                    Task.Run(new Action(SendLoop));
                    Task.Run(new Action(XBeeReceiveLoop));
                }
                else
                {
                    transmitLogger.WriteLine("xBee serial transmit port not aviable", "ERROR", 4);
                }

                if (Contains(SerialPort.GetPortNames(), "/dev/ttyUSB1"))
                {
                    gpsPort = new SerialPort("/dev/ttyUSB1", 9600, Parity.None, 8, StopBits.One);
                    gpsPort.Open();
                    Task.Run(new Action(GpsLoop));
                }
                if (Contains(SerialPort.GetPortNames(), "/dev/ttyAMA0") && false)
                {
                    piPort = new SerialPort("/dev/ttyAMA0", 115200, Parity.None, 8, StopBits.One);
                    receiveLogger.WriteLine("Open com port", "INFO", 1);
                    piPort.Open();
                    carClient = new CanNetworkClient(piPort);
                    Task.Run(new Action(ReceiveLoop));
                }
                else
                {
                    receiveLogger.WriteLine("Reciver not aviable, starting sim mode", "ERROR", 3);
                    carClient = new LocalNetworkClient(163) { ShortMode = true };
                    
                    Task.Run(new Action(ReceiveLoop));
                }
            }
            catch (Exception e)
            {
                receiveLogger.WriteException("Error opening connection", e, 4);
                //xbeePort.Dispose();
            }
        }

        public static bool Contains(string[] devices, string dev)
        {
            foreach (string s in devices)
            {
                if (s == dev)
                    return true;
            }
            return false;
        }

        public static void ReceiveLoop()
        {
            receiveLogger.WriteLine("Started car receive loop", "INFO", 1);
            int packagesReceived = 0;
            try
            {
                while (true)
                {

                    DataWrapper[] data = carClient.ReadDataWrappers(false);
                    packagesReceived++;
                    //usartLogWriter.Write(carClient.LastRecivedBytes);
                    AddValue(data);

                    //receiveLogger.WriteLine("Recved message, bytes left to read: " + piPort.BytesToRead + " package count: " + packagesReceived, "INFO", 1);
                }
            }
            catch (Exception e)
            {
                receiveLogger.WriteException("There has been an error in the recive loop", e, 4);
            }
        }

        public static void SendLoop()
        {
            transmitLogger.WriteLine("Creating trasmit client", "INFO", 1);
            int counter = 0;
            int packagePerSec = 5;
            transmitLogger.WriteLine("Startet trasmit loop", "INFO", 1);
            while (xbeePort.CtsHolding)
            {
                //byte[] tempArray = new byte[3 * (counter % 5 == 0 ? 8 : 2)];
                List<DataWrapper> wrappers = new List<DataWrapper>();

                if (sendFullGps)
                {
                    DataWrapper? degWrap = GetData(200);
                    DataWrapper? minWrap = GetData(201);
                    if (degWrap != null)
                        wrappers.Add(degWrap.Value);
                    if (minWrap != null)
                        wrappers.Add(minWrap.Value);
                    sendFullGps = false;
                }

                for (ushort i = 0; i < SensorLookup.SensorIDs.Length; i++)
                {
                    DataWrapper? wrapper = GetData(i);
                    if (wrapper != null)
                    {
                        if (counter % packagePerSec < (SensorLookup.GetById(i).Importance * packagePerSec - 0.5))
                            wrappers.Add(wrapper.Value);
                    }
                }
                try
                {
                    telemetryClient.SendDataWrappers(wrappers.ToArray());
                    transmitLogger.WriteLine("Sent message nr: " + counter, "INFO", 1);
                    System.Threading.Thread.Sleep(1000 / packagePerSec);
                }
                catch (Exception e)
                {
                    transmitLogger.WriteException("There has been an exception in the send loop", e, 4);
                }
                counter++;
            }
        }

        public static void XBeeReceiveLoop()
        {
            receiveLogger.WriteLine("Started listening", "INFO", 1);
            UdpNetworkClient client = new UdpNetworkClient(xbeePort, 150);
            receiveLogger.WriteLine("Open com port", "INFO", 1);
            int packagesReceived = 0;
            try
            {
                while (true)
                {
                    byte[] bytes = client.Receive();
                    packagesReceived++;
                    usartLogWriter.Write(bytes);

                    if (bytes == new byte[] { 200, 0, 0 })
                    {
                        DriveToSide = true;
                        MessageReceived = DateTime.Now;
                    }

                    for (int i = 0; i < bytes.Length; i += 3)
                    {
                        AddValue(DataWrapper.ReadData(bytes, i, true));
                    }
                }
            }
            catch (Exception e)
            {
                receiveLogger.WriteException("There has been an error in the recive loop", e, 4);
            }
        }

        public static void GpsLoop()
        {
            while (true)
            {
                string line = gpsPort.ReadLine();
                string[] parts = line.Split(',');
                try
                {
                    switch (parts[0].ToUpper())
                    {
                        case "$GPGLL":
                            UpdateGPS(parts[1], parts[3], parts[2], parts[4]);
                            break;
                    }
                }
                catch (Exception e)
                {
                    gpsLogger.WriteException("An exception occured", e, 3);
                }
            }
        }

        private static void AddValue(DataWrapper[] wrapper)
        {
            foreach (DataWrapper dw in wrapper)
            {
                AddValue(dw);
            }
        }

        private static void AddValue(DataWrapper wrapper)
        {
            wrapper.TimeStamp = (int)GameTime.GlobalTime.TotalElapsed.TotalMilliseconds;
            usartLogWriter.Write(wrapper.GetBytesWithTime());
            SensorLookup lookup = SensorLookup.GetById(wrapper.SensorID);
            if (lastesTable.ContainsKey(lookup))
                lastesTable[lookup] = wrapper;
            else
                lastesTable.Add(lookup, wrapper);
            //receiveLogger.WriteLine(string.Format("Message ID: {0} \tValue: {1}", wrapper.SensorID, wrapper.Value), "INFO", 1);

            if (wrapper.SensorID > 99 && wrapper.SensorID < 200)
            {
                exceptionWriter.Write(wrapper.GetBytes(true));
                canLogger.WriteCustom(new CanEntry() { CanMessage = wrapper, Category = "ERROR", Level = 4, Value = "CAN error message" });
            }
        }

        static bool sendFullGps = false;

        static ushort prevdeg;
        static ushort prevmin;
        static ushort prevsecLat;
        static ushort prevsecLong;

        static ushort deg;
        static ushort min;
        static ushort secLat;
        static ushort secLong;


        public static void UpdateGPS(string latitude, string longitude, string dirNS, string dirEW)
        {
            prevdeg = deg;
            prevmin = min;
            prevsecLat = secLat;
            prevsecLong = secLong;

            byte degLat = byte.Parse(latitude.Substring(0, 2));
            ushort degLong = ushort.Parse(longitude.Substring(0, 3));

            byte minLat = byte.Parse(latitude.Substring(2, 2));
            byte minLong = byte.Parse(longitude.Substring(3, 2));

            deg = (ushort)((degLat << 9) | degLong);
            min = (ushort)(((dirNS == "N" ? 0 : 1) << 15) | ((dirEW == "E" ? 0 : 1) << 14) | (minLat << 6) | minLong);
            secLat = ushort.Parse(latitude.Substring(5, 5));
            secLong = ushort.Parse(longitude.Substring(6, 5));

            AddValue(new DataWrapper() { SensorID = 202, Value = secLat });
            AddValue(new DataWrapper() { SensorID = 203, Value = secLong });

            if (prevmin != min || prevdeg != deg)
            {
                AddValue(new DataWrapper() { SensorID = 200, Value = deg });
                AddValue(new DataWrapper() { SensorID = 201, Value = min });
                sendFullGps = true;
            }
        }
    }



    public class OBJWrap<T>
    {
        public T obj { get; set; }

        public OBJWrap(T t)
        {
            this.obj = t;
        }
    }

    public class FileWriter : IDisposable
    {
        byte[] data;
        int currentPlace;
        BinaryWriter bw;// = new TextWriter();
        string path;
        bool opend = false;
        DateTime lastUpdate;
        const int bufferSize = 3000;

        public FileWriter(string filePath)
        {
            data = new byte[bufferSize];
            currentPlace = 0;
            path = filePath;
            lastUpdate = DateTime.Now;
        }

        ~FileWriter()
        {
            if (opend)
            {
                bw.Close();
                opend = false;
            }
        }

        public void Write(byte[] buffer)
        {

            Buffer.BlockCopy(buffer, 0, data, currentPlace, buffer.Length);
            currentPlace += buffer.Length;
            if (currentPlace == data.Length || DateTime.Now > lastUpdate + TimeSpan.FromMilliseconds(500))
            {
                if (!opend)
                {
                    if (!File.Exists(path))
                        File.Create(path).Close();
                    bw = new BinaryWriter(new FileStream(path, FileMode.Append));
                    opend = true;
                }
                bw.Write(data, 0, currentPlace);
                bw.Flush();
                data = new byte[bufferSize];
                currentPlace = 0;
                lastUpdate = DateTime.Now;
            }
        }

        #region IDisposable implementation

        public void Dispose()
        {
            bw.Close();
            opend = false;
        }

        #endregion
    }
}
