﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ion.Graphics.IonEngine;
using Ion.Graphics.IonEngine.Events;
using Ion.Graphics.IonEngine.Drawing;
using Ion.Graphics.IonEngine.Font;
using Ion.Graphics.IonEngine.Gui;
using System.IO;
using Ion.Data.Networking.Manager;
using Ion.Data.Sensor;
using Ion.Data.Networking.Logging;

namespace Ion.Pro.CarGui
{
    class MainWindow : GuiWindow
    {
        public const bool DebugMode = true;
        SDLRenderer renderer;
        public SDLFont digiFont;
        KeyboardState curKey;
        KeyboardState prevKey;
        MouseState prevMouse;
        MouseState curMouse;
        int maxSpeed = 200;
        LogManager mainLogger;
        LogInterface guiLogger;

        //Disable warning about field is never assigned and is never used because thay are assigned via code
        //and many are not used because of GUI Elements
        #pragma warning disable 649, 169
        #region Page0
        ListView listView_event;
        Button button_clear;
        Button button_clearLight;
        #endregion

        #region Page1
        InfoMeter infometer_bat;
        InfoMeter infometer_volt;
        InfoMeter infometer_tempBat;
        InfoMeter infometer_tempCool;
        Speedometer speedometer;
        Label label_Speed;
        Label label_Speed2;
        Label label_voltageMax;
        Label label_voltageMin;
        Label label_batMax;
        Label label_batMin;
        Label label_tempCoolMax;
        Label label_tempCoolMin;
        Label label_tempBatMax;
        Label label_tempBatMin;
        TextBlock textBox_infoBlock;
        Image image_bat;
        Image image_bat12;
        Image image_coolTemp;
        Image image_batTemp;
        Image image_warningBox;
        Image image_startupWarning;
        Image image_breakWarning;
        Image image_temperature;
        Image image_pedalWarning;
        Image image_engineWarning;
        #endregion

        #region Page3
        BarMeter speedPeddalMeter;
        BarMeter breakPeddalMeter;
        BarMeter wheelTurnMeter;
        BarMeter suspensionMeter;
        BarMeter currentBarMeter;
        #endregion

        #pragma warning restore 649, 169

        public MainWindow()
        {
            renderer = SDLRenderer.Create(this);
            mainLogger = new LogManager();
            guiLogger = mainLogger.CreateLogger("GUI");
            
#pragma warning disable
            if (DebugMode)
                mainLogger.OnVerbose += MainLogger_OnWarning;
            else
                mainLogger.OnWarning += MainLogger_OnWarning;
#pragma warning restore

            FileInfo fontFile = new FileInfo("Content/SFDigital.ttf");
            FileInfo font2File = new FileInfo("Content/MonospaceTypewriter.ttf");
            digiFont = SDLFont.LoadFont(fontFile.FullName, 150);
            Global.Font = SDLFont.LoadFont(font2File.FullName, 50);
            Global.SideScroll = true;

            InitializeComponents(renderer);
            DataReciver.Initialize(mainLogger);
            CanError.Initialize();

            ShowCursor = true;
            scroller.CurrentPage = 1;
            FrameLock = true;
            FrameRate = 60;

            infometer_bat.MaxValue = 100;
            infometer_tempBat.MaxValue = 100;
            infometer_tempCool.MaxValue = 100;
            infometer_volt.MaxValue = 500;
        }

        private void MainLogger_OnWarning(object sender, LogWriteEventArgs e)
        {
            if (e.Entry is CanEntry && e.Entry.Sender == "CANMSG")
            {
                CanEntry error = (e.Entry as CanEntry);
                image_warningBox.ImageColor = SDLColor.Red;
                switch (error.CanMessage.SensorID)
                {
                    case 100:
                        image_pedalWarning.ImageColor = SDLColor.Red;
                        break;
                    case 101:
                        image_startupWarning.ImageColor = SDLColor.Red;
                        break;
                    case 102:
                        image_engineWarning.ImageColor = SDLColor.Red;
                        break;
                    case 103:
                        image_engineWarning.ImageColor = SDLColor.Red;
                        break;
                }
                listView_event.Items.Add($"{e.Entry.Time}({e.Entry.Sender})[{e.Entry.Level}]: {error.GetError().Msg}");

            }
            else
            {
                listView_event.Items.Add($"{e.Entry.Time}({e.Entry.Sender})[{e.Entry.Level}]: {e.Entry.Value}");
            }
        }

        public void button_clear_Click(object sender, EventArgs e)
        {
            listView_event.Items.Clear();
        }

        public void button_clearLight_Click(object sender, EventArgs e)
        {
            image_warningBox.ImageColor = new SDLColor(68, 68, 68);
            image_pedalWarning.ImageColor = new SDLColor(68, 68, 68);
            image_startupWarning.ImageColor = new SDLColor(68, 68, 68);
            image_engineWarning.ImageColor = new SDLColor(68, 68, 68);
        }

        public void DoEvents()
        {
            prevKey = curKey;
            prevMouse = curMouse;

            curKey = Keyboard.GetState();
            curMouse = Mouse.GetState();

            //Unused
            MouseData.LastPos = new SDLPoint(-1, -1);

            //Test code to simulate speed and page switch
            if (curKey.IsKeyDown(Keys.UP))
                speedometer.Value += 1;
            else if (curKey.IsKeyDown(Keys.DOWN))
                speedometer.Value -= 1;
            if (curKey.IsKeyDown(Keys.LEFT) && !prevKey.IsKeyDown(Keys.LEFT))
                scroller.CurrentPage--;
            else if (curKey.IsKeyDown(Keys.RIGHT) && !prevKey.IsKeyDown(Keys.RIGHT))
                scroller.CurrentPage++;
        }

        public void UpdateValues()
        {
            ushort[] sensors = SensorLookup.SensorIDs;
            DataWrapper? wrapper = null;

            //Advanced switch body, updates sensor values as it should
            SwitchBody<SensorLookup> switcher = SwitchBody<SensorLookup>.CreateSwitch()
                .Case(SensorLookup.GetByName(SensorLookup.SPEED), () => speedometer.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.SPEED).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.SOC), () => infometer_bat.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.SOC).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.VOLTAGE12), () => infometer_volt.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.VOLTAGE12).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.TEMPBAT), () => infometer_tempBat.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.TEMPBAT).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.TEMPCOOL), () => infometer_tempCool.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.TEMPCOOL).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.SPEEDPEDDAL), () => speedPeddalMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.SPEEDPEDDAL).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.BREAKPEDDAL), () => breakPeddalMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.BREAKPEDDAL).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.DRVWHEEL), () => wheelTurnMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.DRVWHEEL).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.SUSPENSION), () => suspensionMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.SUSPENSION).LowLimit)
                .Case(SensorLookup.GetByName(SensorLookup.CURRENT), () => currentBarMeter.Value = wrapper.Value.Value - SensorLookup.GetByName(SensorLookup.CURRENT).LowLimit)
                ;

            for (ushort i = 0; i < sensors.Length; i++)
            {
                //Get lastes wrapper
                wrapper = DataReciver.GetData(i);

                if (wrapper != null)
                {
                    switcher.Switch(SensorLookup.GetById(i)).Run();
                }
            }
        }

        public override void Update(GameTime gameTime)
        {
            DoEvents();
            UpdateValues();
            label_Speed.TextColor = SDLColor.White;
            label_Speed.Text = (speedometer.Persent * maxSpeed).ToString("000");
            label_Speed2.Text = "";
            scroller.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            renderer.Clear();
            scroller.Draw(renderer, gameTime);
            renderer.Present();
        }
    }
}
